package org.corningrobotics.enderbots.OpenCVTest;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.util.Vector;
import java.util.ArrayList;

/**
 * Runs a GUI for a simple adder.
 * @author Derek Liu
 */
import javax.swing.JOptionPane;

import org.omg.CORBA.portable.ApplicationException;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;

import javafx.application.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.geometry.*;
import javafx.event.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.embed.swing.SwingFXUtils;
import javafx.animation.AnimationTimer;
/**
 * the opencv testbed
 * ---
 * this is a javafx based opencv testbed.
 * By default it opens a video stream defined at compile time and displays it back, 
 * running the image through opencv data strucutres.
 * 
 * It is intended to be developed and run on personal computers to reduce fuss and 
 * facilitate fast development of algorithms.
 * 
 * to install opencv and set it up for this project, this link: 
 * 
 * http://opencv-java-tutorials.readthedocs.io/en/latest/01-installing-opencv-for-java.html
 * 
 * seems of particular use, BUT do note that instead of adding opencv to the global build path, 
 * this particular project adds it to the project-specific build path findable in the project properties.
 * 
 * ---
 * some notes on opencv in general.
 * 
 * Generally, the java bindings resemble c++ in call semantics, except for a few infuriating places 
 * where they differ, so one should still be on alert for those. Google is your friend.
 * 
 * The java libraries are better organized, splitting the c++ "cv::*" namespace into a series of several classes,
 * mostly named after the c++ header files that contain the signatures of the functions they declare.
 * 
 * The python bindings, while still similar in how people will use them in program flow, often have different
 * semantics altogether reflecting the dynamically typed nature of the language over the static typing of java
 * and c++.
 * 
 * It seems fairly trivial to link into Android, and the bindings themselves don't seem to differ from
 * desktop opencv java.
 * 
 * @author Derek Liu
 *
 */
public class EntryPoint extends Application
{
	ImageView imgView;
	Button startButton;
	VideoCapture capture;

	public void start(Stage stage)
	{
		// title
		stage.setTitle("RTSP Stream");
		// declarations
		BorderPane rootNode = new BorderPane();
		Scene scene = new Scene(rootNode);

		// controls boilerplate
		BufferedImage b_img = new BufferedImage(1280, 720, BufferedImage.TYPE_INT_ARGB);
		Graphics2D    graphics = b_img.createGraphics();

		graphics.setPaint(new Color(255, 128, 255));
		graphics.fillRect(0, 0, b_img.getWidth(), b_img.getHeight());
		imgView = new ImageView(SwingFXUtils.toFXImage(b_img, null));
		
		rootNode.setCenter(imgView);
		
		// actual opencv stuff
		// loads the native opencv library
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		
		/* this is where one should define a videocapture to a live video feed
		 * in my particular setup, i used an app on my iphone that made it an RTSP camera server, 
		 * so i put an RTSP addr for my local network here.
		 * for those with webcams (not me) you should take a look at alternate constructors
		 * in most cases using "new VideoCapture(0)" will work just fine.
		 * obviously this will be different in android
		 * 
		 * as opencv uses ffmpeg under the hood, you could probably get away with a wide range of protocols
		 * and video files that this thing will accept
		 */
		capture = new VideoCapture("rtsp://192.168.1.7");
		if (!capture.isOpened()) {
			System.err.println("Error: can't open stream!");
			return;
		}
		new AnimationTimer() {
			/*
			 * where all the logic is
			 * runs at 60fps i would think
			 */
			public void handle(long nanoTime) {
				
				// pretty much the analouge of "frame << capture;" in c++ code
				Mat frame = new Mat();
				if (!capture.read(frame)) {
					return;
				}
				
				Mat hsv = new Mat();
				Mat mask = new Mat();
				Mat mask1 = new Mat();
				Mat mask2 = new Mat();
				
				// make a mask of "red" things from an hsv image
				// decent at filtering red; but it's noisy and also picks up brown
				Imgproc.cvtColor(frame, hsv, Imgproc.COLOR_BGR2HSV);
				Core.inRange(hsv, new Scalar(0, 20, 90), new Scalar(10, 255, 255), mask1);
				Core.inRange(hsv, new Scalar(170, 20, 90), new Scalar(179, 255, 255), mask2);
				Core.bitwise_or(mask1, mask2, mask);

				// apparently one approach to reduce noise; remove all the small things
				//Imgproc.erode(mask, mask, new Mat(), new Point(-1, -1), 2);
				//Imgproc.dilate(mask, mask, new Mat(), new Point(-1, -1), 2);
				
				Mat circles = new Mat();
				
				// contour attempt - apparently tracing contours could be a way to find circles
				// haven't actually gotten it to work/ported over code from python bindings well
				//ArrayList<MatOfPoint> contours = new ArrayList<MatOfPoint>();
				//Imgproc.findContours(mask, contours, new Mat(), Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
				//for (MatOfPoint contour : contours) {
				
				// houghcircles attempt; ended in horrible performance and weird circles being found from noise
				/*
				Imgproc.HoughCircles(mask, circles, Imgproc.HOUGH_GRADIENT, 1, mask.rows()/8, 200, 60, 0, 0);
				for (int i = 0; i < circles.cols(); i++) {
					double[] circle = circles.get(0, i);
					Point center = new Point((int) circle[0], (int) circle[1]);
					int radius = (int) circle[2];
					Imgproc.circle(frame, center, 3, new Scalar(0, 255, 0));
					Imgproc.circle(frame, center, radius, new Scalar(0, 0, 255), 3, 8, 0);
				}*/
				
				// you can use this to apply a mask to colored frames
				//Mat res = new Mat();
				//frame.copyTo(res, mask);
				
				// but for now just display video input
				imShow(frame);
			}
		}.start();
		stage.setScene(scene);
		stage.show();
	}
	
	/**
	 * Since OpenCV does not expose its GUI code for Java, we have a custom function
	 * that updates the javafx imageview with a provided image matrix.
	 * 
	 * Use this instead of all the highgui stuff that doesn't actually work.
	 * However a current limitation is that we can only show one video output at a time
	 * this can be fixed but i'm lazy
	 * @param mat
	 */
	private void imShow(Mat mat) {
		MatOfByte buffer = new MatOfByte();
		Imgcodecs.imencode(".bmp", mat, buffer);
		imgView.setImage(new Image(new ByteArrayInputStream(buffer.toArray())));
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
